import itertools

from digital import bitwidth
from rom import rom
from os import path


def csv_to_rom(filename, out_dir="../output"):
    r = rom()
    with open(filename, 'r') as f:
        r.name = f.readline().replace("\n", "")
        addr = itertools.count()
        r.rows = [{'addr': addr.next(), 'data': int(line)} for line in f]
        maxaddr = max([row['addr'] for row in r.rows])
        datasigned = min([row['data'] for row in r.rows]) < 0
        maxdata = max([row['data'] for row in r.rows])
        r.addr_width = bitwidth(maxaddr, signed=False)
        r.data_width = bitwidth(maxdata, signed=datasigned)
    with open(path.join(out_dir, "{}.v".format(r.name)), 'w+') as output:
        output.write(str(r))
