from __future__ import division
from threshold import Sigmoid, Arctan, Ramp, ThresholdLUT
from digital import DiscreteNum
from os import path
from multiply_accumulate import MultiplyAccumulate


def createThresholdFunction():
    tname = raw_input("Threshold function type: ")

    print "Specify function parameters:"
    fprime0 = float(raw_input("\tf'(0): "))
    fmin = float(raw_input("\tfmin : "))
    fmax = float(raw_input("\tfmax : "))
    print ""

    if (tname == "sigmoid"):
        return Sigmoid(fprime0, fmin, fmax)

    if (tname == "arctan"):
        return Arctan(fprime0, fmin, fmax)

    if (tname == "ramp"):
        return Ramp(fprime0, fmin, fmax)


def createSignalRepresentation(signalName):
    print "Specify the representation for the {}:".format(signalName)
    cmax = float(
        raw_input("\tMaximum value of continuous variable: ")
    )
    width = int(
        raw_input("\tBit width of discrete representation: ")
    )
    rep = DiscreteNum(cmax, width)

    print "The specified representation has the following properties:"
    print "\tResolution: ", rep.resolution()
    print "\tRange:      ", str(rep.crange())
    print ""

    return rep


def createThresholdLut():
    fn = createThresholdFunction()
    sig = createSignalRepresentation("neural output signals")
    wt = createSignalRepresentation("neural weights")
    nct = int(raw_input("Number of neurons in the largest layer: "))

    lut = ThresholdLUT(fn, sig, wt, nct)
    lut.printDescription()

    return lut


def createFunctionalUnit(lut):
    inputs = max(int(raw_input("Number of inputs: ")), lut.neurons)
    return MultiplyAccumulate(lut.signal, lut.weight, lut.neurons, inputs)


def reldir(relpath):
    return path.normpath(path.join(path.dirname(path.abspath(__file__)),
                                   relpath))


lut = createThresholdLut()
fu = createFunctionalUnit(lut)
lut.plot()
out_dir = reldir("../output")
lut.generate_code(out_dir)
fu.generate_code(out_dir)
