import json
import json_decoders


with open("../data/test_network.json", "r") as f:
    lut = json.load(f, object_hook=json_decoders.asThresholdLut)
    lut.printDescription()
    lut.plot()
