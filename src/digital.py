from __future__ import division
from math import log, ceil


def verilog_str(value, width=0):
    if (width == 0):
        width = bitwidth(value)
    if (value < 0):
        return "-{}'d{}".format(int(width), abs(int(value)))
    else:
        return "{}'d{}".format(int(width), int(value))


def discrete(c, M, n):
    return int(round(c * ((2**n)-2)/(2*M)))


def continuous(d, M, n):
    return d * (2*M) / ((2**n)-2)


def bitwidth(dmax, signed=True):
    return int(
        ceil(
            log(abs(dmax) + 1, 2)
        )
    ) + int(signed)


class DiscreteNum:
    def __init__(self, continuousMax, discreteWidth):
        self.__M = continuousMax
        self.__w = int(discreteWidth)

    def discrete(self, c):
        return discrete(c, self.__M, self.__w)

    def continuous(self, d):
        return continuous(d, self.__M, self.__w)

    def resolution(self):
        return self.continuous(1)

    def count(self):
        return 2 ** self.width()

    def width(self):
        return self.__w

    def drange(self):
        return (self.dmin(), self.dmax())

    def crange(self):
        return (self.cmin(), self.cmax())

    def dmax(self):
        return 2 ** (self.__w - 1) - 1

    def dmin(self):
        return -(2 ** (self.__w - 1))

    def cmax(self):
        return self.__M

    def cmin(self):
        return -self.__M - self.resolution()

    def dvals(self):
        return range(self.dmin(), self.dmax() + 1)

    def cvals(self):
        return [val * self.resolution() for val in self.dvals()]

    def verilog_str(self, d):
        return verilog_str(d, self.__w)


class DiscreteUnsignedNum:
    def __init__(self, continuousMax, discreteWidth):
        self.__M = continuousMax
        self.__w = int(discreteWidth + 1)

    def discrete(self, c):
        return discrete(c, self.__M, self.__w)

    def continuous(self, d):
        return continuous(d, self.__M, self.__w)

    def resolution(self):
        return self.continuous(1)

    def count(self):
        return 2 ** self.width()

    def width(self):
        return self.__w - 1

    def drange(self):
        return (0, self.dmax())

    def crange(self):
        return (0, self.cmax())

    def dmax(self):
        return 2 ** (self.__w - 1) - 1

    def dmin(self):
        return 0

    def cmax(self):
        return self.__M

    def cmin(self):
        return 0

    def dvals(self):
        return range(0, self.dmax() + 1)

    def cvals(self):
        return [val * self.resolution() for val in self.dvals()]

    def verilog_str(self, d):
        return verilog_str(d, self.__w - 1)
