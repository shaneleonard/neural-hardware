from __future__ import division
from threshold import Sigmoid, Arctan, Ramp, ThresholdLUT
from digital import DiscreteNum
from multiply_accumulate import MultiplyAccumulate


def asDiscreteNum(dct):
    if ('max-value' in dct and 'bit-width' in dct):
        return DiscreteNum(dct['max-value'], dct['bit-width'])
    return None


def asThresholdFunction(dct):
    if ('type'  in dct and
        'slope' in dct and
        'max'   in dct and
        'min'   in dct):
        if (dct['type'] == 'sigmoid'):
            return Sigmoid(dct['slope'], dct['min'], dct['max'])
        elif (dct['type'] == 'arctan'):
            return Arctan(dct['slope'], dct['min'], dct['max'])
        elif (dct['type'] == 'ramp'):
            return Ramp(dct['slope'], dct['min'], dct['max'])

    return None


def asThresholdLut(dct):
    if ('threshold-function'       in dct and
       'neural-output'             in dct and
       'neural-weight'             in dct and
       'widest-layer-neuron-count' in dct):
        print dct
        fn = asThresholdFunction(dct['threshold-function'])
        no = asDiscreteNum(dct['neural-output'])
        nw = asDiscreteNum(dct['neural-weight'])
        wl = dct['widest-layer-neuron-count']
        return ThresholdLUT(fn, no, nw, wl)
    return dct


def asFunctionalUnit(dct):
    if ('input-count'   in dct and
        'neural-output' in dct and
        'neural-weight' in dct and
        'widest-layer-neuron-count' in dct):
        ic = dct['input-count']
        no = asDiscreteNum(dct['neural-output'])
        nw = asDiscreteNum(dct['neural-weight'])
        wl = dct['widest-layer-neuron-count']
        return MultiplyAccumulate(ic, no, nw, wl)
    return None
