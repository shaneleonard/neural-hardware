#!/usr/bin/env python




##################################################
## DEPENDENCIES
import sys
import os
import os.path
try:
    import builtins as builtin
except ImportError:
    import __builtin__ as builtin
from os.path import getmtime, exists
import time
import types
from Cheetah.Version import MinCompatibleVersion as RequiredCheetahVersion
from Cheetah.Version import MinCompatibleVersionTuple as RequiredCheetahVersionTuple
from Cheetah.Template import Template
from Cheetah.DummyTransaction import *
from Cheetah.NameMapper import NotFound, valueForName, valueFromSearchList, valueFromFrameOrSearchList
from Cheetah.CacheRegion import CacheRegion
import Cheetah.Filters as Filters
import Cheetah.ErrorCatchers as ErrorCatchers

##################################################
## MODULE CONSTANTS
VFFSL=valueFromFrameOrSearchList
VFSL=valueFromSearchList
VFN=valueForName
currentTime=time.time
__CHEETAH_version__ = '2.4.4'
__CHEETAH_versionTuple__ = (2, 4, 4, 'development', 0)
__CHEETAH_genTime__ = 1416338904.262931
__CHEETAH_genTimestamp__ = 'Tue Nov 18 11:28:24 2014'
__CHEETAH_src__ = 'templates/functional_unit_tb.tmpl'
__CHEETAH_srcLastModified__ = 'Thu Oct 16 14:33:12 2014'
__CHEETAH_docstring__ = 'Autogenerated by Cheetah: The Python-Powered Template Engine'

if __CHEETAH_versionTuple__ < RequiredCheetahVersionTuple:
    raise AssertionError(
      'This template was compiled with Cheetah version'
      ' %s. Templates compiled before version %s must be recompiled.'%(
         __CHEETAH_version__, RequiredCheetahVersion))

##################################################
## CLASSES

class functional_unit_tb(Template):

    ##################################################
    ## CHEETAH GENERATED METHODS


    def __init__(self, *args, **KWs):

        super(functional_unit_tb, self).__init__(*args, **KWs)
        if not self._CHEETAH__instanceInitialized:
            cheetahKWArgs = {}
            allowedKWs = 'searchList namespaces filter filtersLib errorCatcher'.split()
            for k,v in KWs.items():
                if k in allowedKWs: cheetahKWArgs[k] = v
            self._initCheetahInstance(**cheetahKWArgs)
        

    def respond(self, trans=None):



        ## CHEETAH: main method generated for this template
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        write(u'''`timescale 1ns / 1ps

module functional_unit_tb;

localparam SIGNAL_WIDTH = ''')
        _v = VFN(VFFSL(SL,"macc.signal",True),"width",False)() # u'$macc.signal.width()' on line 5, col 27
        if _v is not None: write(_filter(_v, rawExpr=u'$macc.signal.width()')) # from line 5, col 27.
        write(u''';
localparam WEIGHT_WIDTH = ''')
        _v = VFN(VFFSL(SL,"macc.weight",True),"width",False)() # u'$macc.weight.width()' on line 6, col 27
        if _v is not None: write(_filter(_v, rawExpr=u'$macc.weight.width()')) # from line 6, col 27.
        write(u''';
localparam SUM_WIDTH    = ''')
        _v = VFN(VFFSL(SL,"macc.sum",True),"width",False)() # u'$macc.sum.width()' on line 7, col 27
        if _v is not None: write(_filter(_v, rawExpr=u'$macc.sum.width()')) # from line 7, col 27.
        write(u''';
localparam COUNT_WIDTH  = ''')
        _v = VFN(VFFSL(SL,"macc",True),"count_width",False)() # u'$macc.count_width()' on line 8, col 27
        if _v is not None: write(_filter(_v, rawExpr=u'$macc.count_width()')) # from line 8, col 27.
        write(u''';

\t// Inputs
\treg clk;
\treg en;
\treg rst;
\treg [COUNT_WIDTH  - 1 : 0] input_count;
\treg [SIGNAL_WIDTH - 1 : 0] signal;
\treg [WEIGHT_WIDTH - 1 : 0] weight;

\t// Outputs
\twire [SUM_WIDTH - 1 : 0] weighted_sum;
\twire sum_valid, final_input;

\t// Instantiate the Unit Under Test (UUT)
\tfunctional_unit uut (
\t\t.clk          ( clk          ),
\t\t.en           ( en           ),
\t\t.rst          ( rst          ),
\t\t.input_count  ( input_count  ),
\t\t.signal       ( signal       ),
\t\t.weight       ( weight       ),
\t\t.weighted_sum ( weighted_sum ),
\t\t.sum_valid    ( sum_valid    ),
        .final_input  ( final_input  )
\t);

    initial begin
        $dumpfile("functional_unit.vcd");
        $dumpvars;
    end

\tinitial begin
\t\t// Initialize Inputs
\t\ten = 1;
\t\trst = 1;
\t\tinput_count = ''')
        _v = VFFSL(SL,"macc.inputs",True) # u'$macc.inputs' on line 44, col 17
        if _v is not None: write(_filter(_v, rawExpr=u'$macc.inputs')) # from line 44, col 17.
        write(u''';
\t\tweight = 1;

\t\trepeat (10) @(posedge clk);
        @(posedge clk) rst <= 0;
        

        #500;
        @(negedge final_input) en <= 0;
        #500;
        en = 1;
        #500;

        $finish;
\tend

    initial begin
        clk = 0;
        forever #5 clk = ~clk;
    end

    reg [SIGNAL_WIDTH - 1 : 0] signal_prev;

    always @(posedge clk) begin
        if (rst) begin
            signal <= 5;
            signal_prev <= 6;
        end else if (final_input | sum_valid) begin
            signal <= 0;
            signal_prev <= signal_prev + final_input;
        end else begin
            signal <= en ? signal_prev : 0;
            signal_prev <= signal_prev;
        end
    end

endmodule

''')
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        
    ##################################################
    ## CHEETAH GENERATED ATTRIBUTES


    _CHEETAH__instanceInitialized = False

    _CHEETAH_version = __CHEETAH_version__

    _CHEETAH_versionTuple = __CHEETAH_versionTuple__

    _CHEETAH_genTime = __CHEETAH_genTime__

    _CHEETAH_genTimestamp = __CHEETAH_genTimestamp__

    _CHEETAH_src = __CHEETAH_src__

    _CHEETAH_srcLastModified = __CHEETAH_srcLastModified__

    _mainCheetahMethod_for_functional_unit_tb= 'respond'

## END CLASS DEFINITION

if not hasattr(functional_unit_tb, '_initCheetahAttributes'):
    templateAPIClass = getattr(functional_unit_tb, '_CHEETAH_templateClass', Template)
    templateAPIClass._addCheetahPlumbingCodeToClass(functional_unit_tb)


# CHEETAH was developed by Tavis Rudd and Mike Orr
# with code, advice and input from many other volunteers.
# For more information visit http://www.CheetahTemplate.org/

##################################################
## if run from command line:
if __name__ == '__main__':
    from Cheetah.TemplateCmdLineIface import CmdLineIface
    CmdLineIface(templateObj=functional_unit_tb()).run()


