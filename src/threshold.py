from __future__ import division
from digital import DiscreteNum, DiscreteUnsignedNum
from math import ceil, log, floor, exp, pi, atan2, tan
from os import path
from threshold_function import threshold_function
from threshold_function_tb import threshold_function_tb
from rom import rom
import matplotlib.pyplot as plt
import numpy as np


class Sigmoid:
    def __init__(self, fprime0, fmin, fmax):
        self.fprime0 = fprime0
        self.fmin = fmin
        self.fmax = fmax
        self.fR = fmax - fmin

    def activate(self, x):
        return self.fR / (1 + exp(-4*self.fprime0/self.fR * x)) + self.fmin

    def name(self):
        return "sigmoid"


class Arctan:
    def __init__(self, fprime0, fmin, fmax):
        self.fprime0 = fprime0
        self.fmin = fmin
        self.fmax = fmax
        self.fR = fmax - fmin

    def activate(self, x):
        return self.fR / pi \
            * atan2(pi*self.fprime0*x, self.fR) \
            + (self.fmax + self.fmin) / 2

    def name(self):
        return "arctan"


class Ramp:
    def __init__(self, fprime0, fmin, fmax):
        self.fprime0 = fprime0
        self.fmin = fmin
        self.fmax = fmax
        self.fR = fmax - fmin

    def activate(self, x):
        if (x <= -self.fR / (2 * self.fprime0)):
            return self.fmin

        elif (x < self.fR / (2 * self.fprime0)):
            return self.fprime0*x + (self.fmax + self.fmin)/2

        else:
            return self.fmax

    def name(self):
        return "ramp"


class ThresholdLUT:
    def __init__(self, f, signal, weight, neurons, fnInputWidth=-1):
        self.signal = signal
        self.weight = weight
        self.neurons = neurons
        self.f = f

        sumWidth = ceil(
            log(
                self.neurons * (2**(self.signal.width() - 1) - 1)
                * (2**(self.weight.width() - 1) - 1),
                2
            )
        ) + 1

        sumMax = self.neurons * self.signal.cmax() * self.weight.cmax()
        self.sum = DiscreteNum(sumMax, sumWidth)

        if (fnInputWidth != -1):
            optimalFnInputWidth = fnInputWidth
        else:
            optimalFnInputWidth = int(
                ceil(
                    log(
                        abs(self.f.fprime0)*self.neurons*self.weight.cmax()
                        * (2**self.sum.width())
                        * (2**self.signal.width() - 2)
                        / (2**self.sum.width() - 2),
                        2
                    )
                )
            )
        self.functionInput = DiscreteNum(sumMax, optimalFnInputWidth)

        tableInputWidth = min(
            int(
                ceil(
                    log(
                        self.imax() - self.imin() + 1,
                        2
                    )
                )
            ),
            optimalFnInputWidth
        ) - 1
        tableInputMax = (2 ** (tableInputWidth) - 1) * \
            self.functionInput.resolution()
        self.tableInput = DiscreteUnsignedNum(tableInputMax, tableInputWidth)

    def fmaxd(self):
        return self.lookup(self.imax() + 50)

    def fmind(self):
        return self.lookup(self.imin() - 50)

    def clookup(self, x):
        ms = self.sum.width() - self.functionInput.width()
        sdi = 2**(ms - 1) / 2 + x * 2**ms
        sci = self.sum.continuous(sdi)
        fsci = self.f.activate(sci)
        return fsci

    def lookup(self, x):
        fsci = self.clookup(x)
        fsdi = self.signal.discrete(fsci)
        return fsdi

    def table(self):
        return [{'addr': i, 'data': self.lookup(i)}
                for i in self.tableInput.dvals()]

    def imax(self):
        fR = self.f.fR
        fprime0 = abs(self.f.fprime0)
        fmax = self.f.fmax
        fmin = self.f.fmin

        Ms = self.sum.cmax()
        Ns = self.sum.width()
        Mz = self.signal.cmax()
        nz = self.signal.width()
        ms = self.sum.width() - self.functionInput.width()

        xcsigtop = -fR * (2 ** Ns - 2) / (4 * fprime0 * Ms)
        xctantop = fR * (2 ** Ns - 2) / (pi * fprime0 * Ms)
        xcramptop = (2 ** Ns - 2) / (fprime0 * Ms)
        xca = (2 ** nz - 2) / Mz
        xcmax = floor(fmax * xca)
        xavg = (fmax + fmin) / 2
        xmaxbottom = (xcmax + (xcmax % 2) - 1) / xca
        xmaxlogtop = log(fR / (xmaxbottom - fmin) - 1)
        xmaxtantop = tan(pi / fR * (xmaxbottom - xavg))
        xmaxramptop = (xmaxbottom - xavg)
        sigtop = (xcsigtop * xmaxlogtop + 1)
        tantop = (xctantop * xmaxtantop + 1)
        ramptop = (xcramptop * xmaxramptop + 1)
        bottom = 2**(ms + 1)

        if (self.f.name() == "sigmoid"):
            return int(ceil(sigtop / bottom - 3/2))

        if (self.f.name() == "arctan"):
            return int(ceil(tantop / bottom - 3/2))

        if (self.f.name() == "ramp"):
            return int(ceil(ramptop / bottom - 3/2))

        return 0

    def imin(self):
        fR = self.f.fR
        fprime0 = abs(self.f.fprime0)
        fmin = self.f.fmin
        fmax = self.f.fmax

        Ms = self.sum.cmax()
        Ns = self.sum.width()
        Mz = self.signal.cmax()
        nz = self.signal.width()
        ms = self.sum.width() - self.functionInput.width()

        xcsigtop = -fR * (2 ** Ns - 2) / (4 * fprime0 * Ms)
        xctantop = fR * (2 ** Ns - 2) / (pi * fprime0 * Ms)
        xcramptop = (2 ** Ns - 2) / (fprime0 * Ms)
        xca = (2 ** nz - 2) / Mz
        xcmin = floor(fmin * xca)
        xavg = (fmax + fmin) / 2
        xminbottom = (xcmin + (xcmin % 2) + 1) / xca
        xminlogtop = log(fR / (xminbottom - fmin) - 1)
        xmintantop = tan(pi / fR * (xminbottom - xavg))
        xminramptop = (xminbottom - xavg)
        sigtop = (xcsigtop * xminlogtop + 1)
        tantop = (xctantop * xmintantop + 1)
        ramptop = (xcramptop * xminramptop + 1)
        bottom = 2**(ms + 1)

        if (self.f.name() == "sigmoid"):
            return int(floor(sigtop / bottom + 1/2))

        if (self.f.name() == "arctan"):
            return int(floor(tantop / bottom + 1/2))

        if (self.f.name() == "ramp"):
            return int(floor(ramptop / bottom + 1/2))

        return 0

    def tablesz(self):
        return self.tableInput.count() * self.signal.width()

    def generate_code(self, out_dir="../output/"):
        fn = threshold_function()
        fn.lut = self

        table = rom()
        table.name = "threshold_rom"
        table.addr_width = self.tableInput.width()
        table.data_width = self.signal.width()
        table.rows = self.table()

        testbench = threshold_function_tb()
        testbench.lut = self

        with open(path.join(out_dir, "threshold_function.v"), "w") as f:
            f.write(str(fn))
        with open(path.join(out_dir, "threshold_rom.v"), "w") as f:
            f.write(str(table))
        with open(path.join(out_dir, "threshold_function_tb.v"), "w") as f:
            f.write(str(testbench))

    def plot(self):
        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        ax2 = ax1.twinx()

        (x_min, x_max) = self.tableInput.drange()

        xd = np.array(self.tableInput.dvals())
        yd = [self.lookup(x) for x in xd]

        xs = np.arange(self.functionInput.dmin(),
                       self.functionInput.dmax(), 0.05)
        ys = [self.clookup(x) for x in xs]

        xl = np.setdiff1d(self.functionInput.dvals(),
                          self.tableInput.dvals())
        yl = [self.lookup(x) for x in xl]

        ax1.plot(self.functionInput.resolution() * xd, yd, 'bo',
                 self.functionInput.resolution() * xl, yl, 'r^')
        ax2.plot(self.functionInput.resolution() * xs, ys, 'k')

        ax1.set_title("Neuron Threshold Function")
        ax1.set_xlabel("Continuous Input")
        ax1.set_ylabel("Discrete Output")
        ax2.set_ylabel("Continuous Output")

        dscale = 0.1 * (self.fmaxd() - self.fmind())
        cscale = 0.1 * abs(float(self.f.fR))

        yd_max = self.fmaxd() + dscale
        yd_min = self.fmind() - dscale

        ys_max = self.f.fmax + cscale
        ys_min = self.f.fmin - cscale

        ax1.vlines(self.functionInput.crange(),
                   yd_min, yd_max, 'k', 'dashed')
        ax1.vlines(self.tableInput.crange(),
                   yd_min, yd_max, 'r', 'dashed')

        ax2.set_ybound(ys_min, ys_max)
        ax1.set_ybound(yd_min, yd_max)

        ax1.grid(True)
        plt.show()

    def printDescription(self):
        print "The threshold LUT has the following properties: "
        print "\tAddress width           : {} bits".format(self.tableInput.width())
        print "\tData width              : {} bits".format(self.signal.width())
        print "\tNumber of addresses     : {}".format(self.tableInput.count())
        print "\tTable size              : {} bits".format(self.tablesz())
        print "\tFull weighted sum input : {}".format(self.sum.cmax())
        print "\tFull weighted sum width : {}".format(self.sum.width())
        print "\tTruncated sum width     : {}".format(self.functionInput.width())
        print "\tTruncated sum resolution: {}".format(self.functionInput.resolution())
        print ""

    def printFullDescription(self):
        print """
Each weight represents a continuous value in the range {weight_range},
encoded using {weight_width}-bit 2's complement.

Each neural input represents a continuous value in the range {neural_range},
encoded using {neural_width}-bit 2's complement.
This applies to neural outputs as well.

Since there are {max_fan_in} neurons in the largest layer, the maximum
weighted sum value is {max_weighted_sum}. This can be represented using
{max_weighted_sum_width} bits without losing any mathematical precision.

However, this level of precision is unnecessary, because the discrete
threshold function introduces quantization error. Therefore, the weighted
sum can be reasonably approximated using {weighted_sum_opt_width} bits.
These bits are the inputs to the threshold function, and they represent
the continuous range {weighted_sum_range}.

The threshold function is a {threshold_type} function, with a continuous
output range of {threshold_range}.
""".format(
            weight_range=self.weight.crange(),
            weight_width=self.weight.width(),
            neural_range=self.signal.crange(),
            neural_width=self.signal.width(),
            max_fan_in=self.neurons,
            max_weighted_sum=self.sum.cmax(),
            max_weighted_sum_width=self.sum.width(),
            weighted_sum_opt_width=self.functionInput.width(),
            weighted_sum_range=self.sum.crange(),
            threshold_type=self.f.name(),
            threshold_range=self.signal.crange()
        )

        if (self.tableInput.width() < self.functionInput.width()):
            percent_logic = int(
                100 * (1-float(self.tableInput.count() /
                               (2 ** self.functionInput.width())))
            )
            print """
When the threshold function is quantized, ~{percent_logic}% of the outputs are
fmin or fmax. Therefore, it is only necessary to store the central, non-extreme
values in the LUT. These values can be addressed using {lut_addr_width} bits.
""".format(
                percent_logic=percent_logic,
                lut_addr_width=self.tableInput.width()
            )

        else:
            print """
When the threshold function is quantized, the maximum weighted sum value is not
large enough to produce an output of fmax. Therefore, the entire function must
be stored in the LUT, which is addressed using {lut_addr_width} bits.
""".format(lut_addr_width=self.tableInput.width())

        print """
Therefore, the LUT has an address width of {lut_addr_width} bits, a data width
of {neural_width} bits, and a total size of {table_size} bits.
""".format(
            lut_addr_width=self.tableInput.width(),
            neural_width=self.signal.width(),
            table_size=self.tablesz()
        )
