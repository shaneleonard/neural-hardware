#!/usr/bin/env python




##################################################
## DEPENDENCIES
import sys
import os
import os.path
try:
    import builtins as builtin
except ImportError:
    import __builtin__ as builtin
from os.path import getmtime, exists
import time
import types
from Cheetah.Version import MinCompatibleVersion as RequiredCheetahVersion
from Cheetah.Version import MinCompatibleVersionTuple as RequiredCheetahVersionTuple
from Cheetah.Template import Template
from Cheetah.DummyTransaction import *
from Cheetah.NameMapper import NotFound, valueForName, valueFromSearchList, valueFromFrameOrSearchList
from Cheetah.CacheRegion import CacheRegion
import Cheetah.Filters as Filters
import Cheetah.ErrorCatchers as ErrorCatchers

##################################################
## MODULE CONSTANTS
VFFSL=valueFromFrameOrSearchList
VFSL=valueFromSearchList
VFN=valueForName
currentTime=time.time
__CHEETAH_version__ = '2.4.4'
__CHEETAH_versionTuple__ = (2, 4, 4, 'development', 0)
__CHEETAH_genTime__ = 1416338904.287144
__CHEETAH_genTimestamp__ = 'Tue Nov 18 11:28:24 2014'
__CHEETAH_src__ = 'templates/threshold_function_tb.tmpl'
__CHEETAH_srcLastModified__ = 'Sun Aug 31 17:18:52 2014'
__CHEETAH_docstring__ = 'Autogenerated by Cheetah: The Python-Powered Template Engine'

if __CHEETAH_versionTuple__ < RequiredCheetahVersionTuple:
    raise AssertionError(
      'This template was compiled with Cheetah version'
      ' %s. Templates compiled before version %s must be recompiled.'%(
         __CHEETAH_version__, RequiredCheetahVersion))

##################################################
## CLASSES

class threshold_function_tb(Template):

    ##################################################
    ## CHEETAH GENERATED METHODS


    def __init__(self, *args, **KWs):

        super(threshold_function_tb, self).__init__(*args, **KWs)
        if not self._CHEETAH__instanceInitialized:
            cheetahKWArgs = {}
            allowedKWs = 'searchList namespaces filter filtersLib errorCatcher'.split()
            for k,v in KWs.items():
                if k in allowedKWs: cheetahKWArgs[k] = v
            self._initCheetahInstance(**cheetahKWArgs)
        

    def respond(self, trans=None):



        ## CHEETAH: main method generated for this template
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        write(u'''`timescale 1ns / 1ps

module threshold_function_tb();
    
    localparam SUM_WIDTH    = ''')
        _v = VFN(VFFSL(SL,"lut.sum",True),"width",False)() # u'$lut.sum.width()' on line 5, col 31
        if _v is not None: write(_filter(_v, rawExpr=u'$lut.sum.width()')) # from line 5, col 31.
        write(u''';
    localparam SIGNAL_WIDTH = ''')
        _v = VFN(VFFSL(SL,"lut.signal",True),"width",False)() # u'$lut.signal.width()' on line 6, col 31
        if _v is not None: write(_filter(_v, rawExpr=u'$lut.signal.width()')) # from line 6, col 31.
        write(u''';
    localparam [SUM_WIDTH - 1:0] SUM_MIN = 1 << SUM_WIDTH - 1;
    localparam [SUM_WIDTH - 1:0] SUM_MAX = ~SUM_MIN;

    // Inputs
    reg clk;
    reg en;
    reg signed [SUM_WIDTH - 1:0] weighted_sum;

    // Outputs
    wire signed [SIGNAL_WIDTH - 1:0] signal;
    wire signal_valid;

    // Instantiate the Unit Under Test (UUT)
    threshold_function uut (
        .clk          ( clk          ) ,
        .en           ( en           ) ,
        .weighted_sum ( weighted_sum ) ,
        .signal       ( signal       ) ,
        .signal_valid ( signal_valid )
    );

    // Generate clock
    initial begin
        clk = 0;
        forever #5 clk = ~clk;
    end

    initial begin
        $dumpfile("threshold_function.vcd");
        $dumpvars;
    end

    event increment_sum;
    event increment_done;
    event reset_sum;

    reg incrementing;
    task sweep_sum;
    begin
        weighted_sum = SUM_MIN;
        while (incrementing) begin
            #30 -> increment_sum;
        end
        -> reset_sum;
    end
    endtask
    
    initial begin: increment_sum_handler
        forever begin 
            @(increment_sum);
            if (weighted_sum < $signed(SUM_MAX - 7)) begin
                @(negedge clk) weighted_sum <= weighted_sum + 7;
                # 100;
            end else begin
                incrementing = 0; 
            end
        end
    end
    
    initial begin: reset_sum_handler
        forever begin 
            @(reset_sum);
            @(negedge clk) weighted_sum <= 0;
        end
    end

    initial begin
        incrementing = 1;
        en           = 1;
        sweep_sum();
        $finish;
    end
    
endmodule
''')
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        
    ##################################################
    ## CHEETAH GENERATED ATTRIBUTES


    _CHEETAH__instanceInitialized = False

    _CHEETAH_version = __CHEETAH_version__

    _CHEETAH_versionTuple = __CHEETAH_versionTuple__

    _CHEETAH_genTime = __CHEETAH_genTime__

    _CHEETAH_genTimestamp = __CHEETAH_genTimestamp__

    _CHEETAH_src = __CHEETAH_src__

    _CHEETAH_srcLastModified = __CHEETAH_srcLastModified__

    _mainCheetahMethod_for_threshold_function_tb= 'respond'

## END CLASS DEFINITION

if not hasattr(threshold_function_tb, '_initCheetahAttributes'):
    templateAPIClass = getattr(threshold_function_tb, '_CHEETAH_templateClass', Template)
    templateAPIClass._addCheetahPlumbingCodeToClass(threshold_function_tb)


# CHEETAH was developed by Tavis Rudd and Mike Orr
# with code, advice and input from many other volunteers.
# For more information visit http://www.CheetahTemplate.org/

##################################################
## if run from command line:
if __name__ == '__main__':
    from Cheetah.TemplateCmdLineIface import CmdLineIface
    CmdLineIface(templateObj=threshold_function_tb()).run()


