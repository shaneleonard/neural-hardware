from digital import DiscreteNum
from math import ceil, log
from functional_unit import functional_unit
from functional_unit_tb import functional_unit_tb
from os import path


class MultiplyAccumulate:
    def __init__(self, signal, weight, neurons, inputs):
        self.signal = signal
        self.weight = weight
        self.neurons = neurons
        self.inputs  = inputs

        sumWidth = ceil(
            log(
                self.neurons * (2**(self.signal.width() - 1) - 1)
                * (2**(self.weight.width() - 1) - 1),
                2
            )
        ) + 1

        sumMax = self.neurons * self.signal.cmax() * self.weight.cmax()
        self.sum = DiscreteNum(sumMax, sumWidth)

    def count_width(self):
        return int(ceil(log(self.inputs + 1, 2)))

    def generate_code(self, out_dir="../output/"):
        fu = functional_unit()
        futb = functional_unit_tb()
        fu.macc = self
        futb.macc = self

        with open(path.join(out_dir, "functional_unit.v"), "w") as f:
            f.write(str(fu))
        with open(path.join(out_dir, "functional_unit_tb.v"), "w") as f:
            f.write(str(futb))

