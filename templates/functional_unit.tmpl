module functional_unit(
    input clk,
    input en,
    input rst,
    input  [COUNT_WIDTH   - 1 : 0] input_count,
    input  [SIGNAL_WIDTH  - 1 : 0] signal,
    input  [WEIGHT_WIDTH  - 1 : 0] weight,
    output [SUM_WIDTH     - 1 : 0] weighted_sum,
    output reg sum_valid, 
    output reg final_input
);

parameter SIGNAL_WIDTH = $macc.signal.width();
parameter WEIGHT_WIDTH = $macc.weight.width();
parameter SUM_WIDTH    = $macc.sum.width();
parameter COUNT_WIDTH  = $macc.count_width();

reg  [COUNT_WIDTH - 1 : 0] count;
wire [SUM_WIDTH   - 1 : 0] internal_sum;
reg  [SUM_WIDTH   - 1 : 0] prev_weighted_sum;
reg new_dataset;
reg en_delayed;

always @(posedge clk) begin
    if (rst) begin
        count <= 0;
    end else if (en & !final_input) begin
        count <= (count + 1) % (input_count + 2);
    end else begin
        count <= count;
    end
end

always @(posedge clk) begin
    final_input <= (count == input_count);
    new_dataset <= rst | final_input;
    sum_valid   <= !rst & en & final_input;
    en_delayed  <= en;
end

MACC_MACRO #(
    .DEVICE  ( "SPARTAN6"     ),    // Target Device: "VIRTEX5", "VIRTEX6", "SPARTAN6"
    .LATENCY ( 1              ),    // Desired clock cycle latency, 1-4
    .WIDTH_A ( SIGNAL_WIDTH   ),    // Multiplier A-input bus width, 1-25
    .WIDTH_B ( WEIGHT_WIDTH   ),    // Multiplier B-input bus width, 1-18
    .WIDTH_P ( SUM_WIDTH      )     // Accumulator output bus width, 1-48
) MACC_MACRO (
    .P         ( internal_sum ),    // MACC output bus, width determined by WIDTH_P parameter
    .A         ( signal       ),    // MACC input A bus, width determined by WIDTH_A parameter
    .ADDSUB    ( 1'b1         ),    // 1-bit add/sub input, high selects add, low selects subtract
    .B         ( weight       ),    // MACC input B bus, width determined by WIDTH_B parameter
    .CARRYIN   ( 1'b0         ),    // 1-bit carry-in input to accumulator
    .CE        ( en_delayed   ),    // 1-bit active high input clock enable
    .CLK       ( clk          ),    // 1-bit positive edge clock input
    .LOAD      ( 1'b0         ),    // 1-bit active high input load accumulator enable
    .LOAD_DATA ( internal_sum ),    // Load accumulator input data, width determined by WIDTH_P parameter
    .RST       ( new_dataset  )     // 1-bit input active high reset
);

always @(posedge clk) begin
    if (rst)
        prev_weighted_sum <= 0;
    else
        prev_weighted_sum <= weighted_sum;
end

assign weighted_sum = sum_valid ? internal_sum : prev_weighted_sum;

endmodule

