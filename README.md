neural-hardware
===============

Overview
--------

neural-hardware is a Python-based tool that generates RTL code for neural networks 
on FPGAs (field programmable gate arrays). Neural networks are useful in a wide 
variety of machine learning applications, from computer vision to handwriting 
recognition to speech detection. Whether you are a [Papilio](http://papilio.cc) or
[Mojo](http://embeddedmicro.com) hobbyist, or a hardware design professional,
neural-hardware can add some powerful new capabilities to your project.

Current Status
--------------

Project development is currently still in the early stages. The first iteration
will be able to generate all of the necessary Verilog code for a user-specified
neural network. The first target platform is the Xilinx SPARTAN 6, the FPGA 
currently at the heart of the [Papilio](http://papilio.cc) hobby boards.


© 2014 Shane Leonard
