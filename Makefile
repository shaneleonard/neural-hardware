all: templates threshold

.PHONY: templates threshold clean
templates:
	cheetah compile --nobackup --idir=templates --odir=src -R

threshold:
	python src/generate_neural_network.py

clean:
	rm -f src/*.pyc
	$(MAKE) -C output clean
